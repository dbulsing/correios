

# Consulta CEP 

Projeto em WSO2 EI para realizar consulta do CEP no Web Services dos Correios.

A API abstrai a complexidade do acesso SOAP ao serviços dos Correios usando apenas uma consulta REST através do endereço:

http://localhost:8280/cep/{CEP}

Basta enviar o número do CEP na URL e escolher o tipo do da de retorno, se será XML ou JSON, através dos headers:

```
Accept: application/xml
Accept: application/json
```

Por padrão, será retornado como XML.